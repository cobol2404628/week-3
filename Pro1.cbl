       IDENTIFICATION DIVISION. 
       PROGRAM-ID.PRO1.
       AUTHOR. JULLADIT.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01 NUM1         PIC 99.
       01 NUM2         PIC 99.
       01 NUM3         PIC 99.
       01 NUM4         PIC 99.
       01 PROBLEM-STR  PIC X(50).
       
       PROCEDURE DIVISION .
           PERFORM PROBLEM1 
           PERFORM PROBLEM2 
           PERFORM PROBLEM3 
           PERFORM PROBLEM4 
           PERFORM PROBLEM5 
           PERFORM PROBLEM6 
           PERFORM PROBLEM7 
           PERFORM PROBLEM8 
           PERFORM PROBLEM9
           PERFORM PROBLEM10
           PERFORM PROBLEM11
           GOBACK 
           .
       PROBLEM1.
           MOVE "PROBLEM1 : NUM1 && NUM2" TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 25 TO NUM1 
           MOVE 30 TO NUM2 
           MOVE ZERO TO NUM3 
           MOVE ZERO TO NUM4 
          
           PERFORM DISPLAYBEFORE
           ADD NUM1 TO NUM2
           PERFORM DISPLAYAFTER.
           EXIT 
           .
       PROBLEM2.
           MOVE "PROBLEM2 : NUM1 && NUM2 TO NUM3 && NUM4" TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 13 TO NUM1 
           MOVE 04 TO NUM2 
           MOVE 05 TO NUM3 
           MOVE 12 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           ADD NUM1, NUM2 TO NUM3, NUM4
           PERFORM DISPLAYAFTER.
           EXIT 
           . 
       PROBLEM3.
           MOVE "PROBLEM3 : ADD NUM1, NUM2, NUM3 GIVE NUM4"
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 04 TO NUM1 
           MOVE 03 TO NUM2 
           MOVE 02 TO NUM3 
           MOVE 01 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           ADD NUM1, NUM2, NUM3 GIVING NUM4
           PERFORM DISPLAYAFTER.
           EXIT 
           .    
       PROBLEM4.
           MOVE "PROBLEM4 : SUBTRACT NUM1 FROM NUM2 GIVING NUM3"
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 04 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           SUBTRACT NUM1 FROM NUM2 GIVING NUM3
           PERFORM DISPLAYAFTER.
           EXIT 
           .     
       PROBLEM5.
           MOVE "PROBLEM5 : SUBTRACT NUM1, NUM2 FROM NUM3"
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           SUBTRACT NUM1, NUM2 FROM NUM3
           PERFORM DISPLAYAFTER.
           EXIT 
           .       
       PROBLEM6.
           MOVE "PROBLEM6 : SSUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4 "
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 20 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4 
           PERFORM DISPLAYAFTER.
           EXIT 
           .    
       PROBLEM7.
           MOVE "PROBLEM7 : MULTIPLY NUM1 BY NUM2  "
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 10 TO NUM1 
           MOVE 05 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           MULTIPLY NUM1 BY NUM2 
           PERFORM DISPLAYAFTER.
           EXIT 
           .
       PROBLEM8.
           MOVE "PROBLEM8 : MULTIPLY NUM1 BY NUM2 GIVING NUM3  "
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 10 TO NUM1 
           MOVE 05 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           MULTIPLY NUM1 BY NUM2 GIVING NUM3
           PERFORM DISPLAYAFTER.
           EXIT 
           .      
       PROBLEM9.
           MOVE "PROBLEM9 : DIVIDE NUM1 INTO NUM2  "
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 05 TO NUM1 
           MOVE 64 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           DIVIDE NUM1 INTO NUM2
           PERFORM DISPLAYAFTER.
           EXIT 
           .    
       PROBLEM10.
           MOVE "PROBLEM10 : IVIDE NUM2 BY NUM1 GIVING NUM3"
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 05 TO NUM1 
           MOVE 64 TO NUM2 
           MOVE 24 TO NUM3 
           MOVE 88 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4 
           PERFORM DISPLAYAFTER.
           EXIT 
           .  
       PROBLEM11.
           MOVE "PROBLEM11 : COMPUTE NUM1 = 5 + 10 * 30 / 2"
              TO PROBLEM-STR 
           PERFORM HEADER
           MOVE 25 TO NUM1 
           MOVE 00 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
          
           PERFORM DISPLAYBEFORE
           COMPUTE NUM1 = 5 +(10 * 30 / 2)
           PERFORM DISPLAYAFTER.
           EXIT 
           .                              
       HEADER.
           DISPLAY "------------------------------------------"
           DISPLAY PROBLEM-STR 
           DISPLAY "      NUM1 NUM2 NUM3 NUM4 "
           EXIT
           .
       DISPLAYBEFORE.
           DISPLAY "BEFORE " NUM1 "   " NUM2 "   " NUM3 "   " NUM4 
           EXIT 
           .
       DISPLAYAFTER.
           DISPLAY "AFTER  " NUM1 "   " NUM2 "   " NUM3 "   " NUM4 
           EXIT 
           .