       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA5.
       AUTHOR. JULLADIT.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 GRADE-DATA  PIC   X(90) VALUE "39030261WORAWIT         8863455
      -    "93B 886352593D+886342193B+886478593C 886481592C+886491591A".
       01 GRADE REDEFINES GRADE-DATA.  
          03 STU-ID              PIC 9(8).
          03 STU-NAME            PIC X(16).
          03 SUB1.
             05 SUB-CODE1        PIC 9(8).
             05 SUB-UMIT1        PIC 9.
             05 SUB-GRADE1       PIC X(2).
          03 SUB2.    
             05 SUB-CODE2        PIC 9(8).
             05 SUB-UMIT2        PIC 9.
             05 SUB-GRADE2       PIC X(2).
          03 SUB3.
             05 SUB-CODE3        PIC 9(8).
             05 SUB-UMIT3        PIC 9.
             05 SUB-GRADE3       PIC X(2).
          03 SUB4.
             05 SUB-CODE4        PIC 9(8).
             05 SUB-UMIT4        PIC 9.
             05 SUB-GRADE4       PIC X(2).
          03 SUB5.
             05 SUB-CODE5        PIC 9(8).
             05 SUB-UMIT5        PIC 9.
             05 SUB-GRADE5       PIC X(2).
          03 SUB6.
             05 SUB-CODE6        PIC 9(8).
             05 SUB-UMIT6        PIC 9.
             05 SUB-GRADE6       PIC X(2).


                66 STUDENT-ID RENAMES STU-ID.
                66 STUDENT-INFO RENAMES STU-ID THRU SUB-UMIT1.
       01 STUCODE REDEFINES GRADE-DATA.
          05 STU-YEAR            PIC 9(2).
          05 FILLER              PIC X(6).
          05 STU-SHORT-NAME      PIC X(3).
      
       PROCEDURE DIVISION.
       BEGIN.
           
           DISPLAY GRADE

           DISPLAY "SUB1 "
           DISPLAY "CODE : " SUB-CODE1 
           DISPLAY "UNIT : " SUB-UMIT1 
           DISPLAY "GRADE : " SUB-GRADE1 
           DISPLAY "-------------------"
           .
           DISPLAY "SUB2 "
           DISPLAY "CODE : " SUB-CODE2 
           DISPLAY "UNIT : " SUB-UMIT2 
           DISPLAY "GRADE : " SUB-GRADE2 
           DISPLAY "-------------------"
           .
           DISPLAY "SUB3 "
           DISPLAY "CODE : " SUB-CODE3 
           DISPLAY "UNIT : " SUB-UMIT3
           DISPLAY "GRADE : " SUB-GRADE3
           DISPLAY "-------------------"
           .
           DISPLAY "SUB4 "
           DISPLAY "CODE : " SUB-CODE4 
           DISPLAY "UNIT : " SUB-UMIT4 
           DISPLAY "GRADE : " SUB-GRADE4 
           DISPLAY "-------------------"
           .
           DISPLAY "SUB5 "
           DISPLAY "CODE : " SUB-CODE5 
           DISPLAY "UNIT : " SUB-UMIT5
           DISPLAY "GRADE : " SUB-GRADE5
           DISPLAY "-------------------"
           .
           DISPLAY "SUB6 "
           DISPLAY "CODE : " SUB-CODE6
           DISPLAY "UNIT : " SUB-UMIT6 
           DISPLAY "GRADE : " SUB-GRADE6
           DISPLAY "-------------------"

           DISPLAY SUB3
           DISPLAY STUDENT-ID 
           DISPLAY STUDENT-INFO 
           DISPLAY STU-YEAR STU-SHORT-NAME 
           .